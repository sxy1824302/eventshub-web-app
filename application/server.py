#!/usr/bin/env python
from functools import partial
import logging
import os
from redis import Redis
import motor
import tornado.ioloop
import tornado.web
from tornado.options import define, options
from tornado import httpserver
import sys
import importlib
from EventsHub.options import define_options
from EventsHub import indexes, cache, application
from EventsHub.event_tags import TAGS
from EventsHub.models.tags import Tag
from  EventsHub.models.base import CreateRole
from  EventsHub.lib.apns_client import *



if __name__ == "__main__":
    define_options(options)
    options.parse_command_line()
    for handler in logging.getLogger().handlers:
        if hasattr(handler, 'baseFilename'):
            print ('Logging to'+handler.baseFilename)
            break


    if options.local_db:
        normal_uri = 'mongodb://localhost:27017/'
    elif options.local_dev:
        normal_uri = 'mongodb://server_user:JKHGA9812&^aj@121.40.173.54:27000/EventsHub'
    else:
        normal_uri = 'mongodb://server_user:JKHGA9812&^aj@127.0.0.1:27000/EventsHub'

        configure({'HOST': 'http://10.168.81.24:7077/'})
        provision('com.uriphium.eventshub', open('/root/eventshub-web-app/dev_cert.pem').read(), 'sandbox')





    if options.debug:#enable autoreload when developing
        options.autoreload = True

    db = motor.MotorClient(normal_uri).EventsHub
    loop = tornado.ioloop.IOLoop.current()

    redis_conn = Redis()


    #events?
    loop.run_sync(partial(cache.startup, db))

    #build index
    if options.rebuild_indexes or options.ensure_indexes:
        indexes.ensure_indexes(db,drop=options.rebuild_indexes)



    this_dir = os.path.dirname(__file__)
    application = application.get_application(this_dir, db, options,redis_conn)
    http_server = httpserver.HTTPServer(application)



    http_server.listen(options.port)
    msg = 'Listening on port %s' % options.port
    logging.info(msg)

    loop.start()
