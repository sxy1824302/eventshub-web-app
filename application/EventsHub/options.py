import functools
import tornado.options


def define_options(option_parser):
    # Debugging
    option_parser.define(
        'debug', default=False, type=bool,
        help="Turn on autoreload and log to stderr",
        callback=functools.partial(enable_debug, option_parser),
        group='Debugging')

    def config_callback(path):
        option_parser.parse_config_file(path, final=False)

    option_parser.define(
        "config", type=str, help="Path to config file",
        callback=config_callback, group='Config file')

    # Application
    option_parser.define('autoreload', type=bool, default=False, group='Application')

    option_parser.define('port', default=5000, type=int, help=(
        "Server port"), group='Application')

    # Startup
    option_parser.define('test_db', type=bool,default=False, group='Startup')
    option_parser.define('local_db', type=bool,default=False, group='Startup')
    option_parser.define('local_dev', type=bool,default=False, group='Startup')


    option_parser.define('ensure_indexes', default=False, type=bool, help=(
        "Ensure collection indexes before starting"), group='Startup')
    option_parser.define('rebuild_indexes', default=True, type=bool, help=(
        "Drop all indexes and recreate before starting"), group='Startup')

    option_parser.define('template_path', type=str,default="templates/", group='Startup')
    option_parser.define(
        'timezone', type=str, default='Asia/Shanghai',
        help="Your timezone name", group='Appearance')


def enable_debug(option_parser, debug):
    if debug:
        option_parser.log_to_stderr = True
        option_parser.autoreload = True
