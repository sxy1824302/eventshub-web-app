import tornado.httpserver
import tornado.ioloop
import tornado.web
import pytz
from  .handler.base import *
from  .handler.app_authentication import *
from  .handler.event import *
from  .handler.event_participants import *
from  .handler.participant import *
from  .handler.home import *
from  .handler.image_files import *
from  .handler.event_city import *
from  .handler.user import *
from  .handler.tags import *
from .handler.event_comment import *
from .handler.web_authenticate import *


from tornado.options import options
from tornado.template import Loader


def get_application(root_dir, db, option_parser,redis_connection):

    settings = { k : v.value() for k, v in option_parser._options.items()}

    usrls = [
            (r"/$",HomeHandler),
            # web page ==========
            (r"/page/join_event/(?P<event_id>[^\/]+)", PageJoinEventHandler),
            (r"/page/event/(?P<event_id>[^\/]+)",PageEventHandler),
            (r"/download/app/ios", AppDownloadHandler),


            (r"/register",RegisterHandler),
            (r"/email_login",EmailLoginHandler),


            # json API ==========
            (r"/registration_other",ThirdPartyRegisterHandler),
            (r"/login",LoginHandler),

            (r"/users/(?P<user_id>[^\/]+)/device$", UserDeviceHandler),
            (r"/users/(?P<user_id>[^\/]+)$", UsersHandler),


            (r"/events/(?P<event_id>[^\/]+)/join",JoinEventHandler),
            (r"/events/(?P<event_id>[^\/]+)/participants/(?P<participant_ref>[^\/]+)",ParticipantHandler),
            (r"/events/(?P<event_id>[^\/]+)/attend",ParticipantAttendHandler),

            (r"/users/(?P<user_id>[^\/]+)/events$", UserEventsHandler),
            (r"/created_events",CreatedEventsHandler),
            (r"/events$",EventsHandler),
            (r"/events/(?P<event_id>[^\/]+)$",EventsOperationHandler),
            (r"/events/(?P<event_id>[^\/]+)/participants$",EventParticipantsHandler),
            (r"/events/(?P<event_id>[^\/]+)/like",EventLikeHandler),

            (r"/events/(?P<event_id>[^\/]+)/comments$",EventCommentsHandler),

            (r"/events/(?P<event_id>[^\/]+)/comments/(?P<comment_id>[^\/]+)$",EventCommentOperatorHandler),


            (r"/events/(?P<event_id>[^\/]+)/poster$",PosterHandler),
            (r"/events/(?P<event_id>[^\/]+)/poster/(?P<poster>[^\/]+)$",PosterHandler),

            (r"/search/event", SearchEventsHandler),

            (r"/profile/photo",AvatarUploadHandler),

            (r"/tags",TagsHandler),
            (r"/liked_events$",LikedEventsHandler),
            (r"/joint_events$",MyJointEventsHandler),

            (r"/recommends",RecommendEventsHandler),

            (r"/cities",CityHandler),

            (r'/thumbnails/(?P<image>[^\/]+).(?P<extension1>png|jpg)/(?P<width>\+?[1-9][0-9]*).(?P<extension2>png|jpg)',TileHandler),

            (r"/images/(.*)",StaticFileHandler, {"path": os.path.join(os.path.expanduser("~"),"files")}),

            (r"/eventshub/icons/(.*)",StaticFileHandler, {"path": os.path.join(os.path.expanduser("~"),"icons")}),
        ]

    return EventsHubApplication(
        usrls,
        db=db,
        xheaders = True,
        tz=pytz.timezone(option_parser.timezone),
        file_path = os.path.join(os.path.expanduser("~"),"files"),
        redis_connection = redis_connection,
        **settings
        )



class EventsHubApplication(tornado.web.Application):


    @property
    def template_loader(self):
        if not hasattr(self, "_template_loader"):
            self._template_loader = Loader(os.path.join(options.theme_path,
                "messages"))
        return self._template_loader

