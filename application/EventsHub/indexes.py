import logging


def ensure_indexes(sync_db, drop=False):
    if drop:
        logging.info('Dropping indexes...')
        sync_db.likes.drop_indexes()
        sync_db.events.drop_indexes()

        sync_db.users.drop_indexes()
        sync_db.user_details.drop_indexes()

        sync_db.participants.drop_indexes()

        sync_db.tags.drop_indexes()

    logging.info('Ensuring indexes...')

    sync_db.users.ensure_index([('email', 1)], unique=True, sparse = True)
    sync_db.users.ensure_index([('phone', 1)], unique=True, sparse = True)
    sync_db.users.ensure_index([('third_party_credentials.$', 1)])

    sync_db.user_details.ensure_index([('user_ref', 1)], unique=True)


    sync_db.login_track.ensure_index([('user_ref', 1)], unique=True)



    sync_db.cities.ensure_index([('name_hash', 1)], unique=True)

    sync_db.tags.ensure_index([ ('tag_id',1)], unique=True)


    sync_db.comments.ensure_index([('event_ref', 1),('comment_date', -1)])
    sync_db.comments.ensure_index([('comment_date', -1)])



    sync_db.events.ensure_index([('date_end', -1)])
    sync_db.events.ensure_index([('date_end', -1),('owner', 1)])
    sync_db.events.ensure_index([('owner', 1)])
    sync_db.events.ensure_index([('title.zh_CN', "text"), ('title.en_US', "text")])
    sync_db.events.ensure_index([('address.city', 1),('date_end', -1)])

    sync_db.likes.ensure_index(('event_ref', 1),unique = True)
    sync_db.likes.ensure_index([('likes.user_ref', 1),('event_ref', 1)])


    sync_db.participants.ensure_index([('event_ref', 1)], unique=True)

    sync_db.notifications.ensure_index([('user_ref', 1)], unique=True)

    logging.info('index work->>done.')
