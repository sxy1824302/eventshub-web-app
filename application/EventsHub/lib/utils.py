__author__ = 'alexander'
from PIL import Image
import os
import fnmatch
EXTENSION_FORMAT = {"gif":'GIF',"jpg":'JPEG', "png":'PNG'}



def enum(**enums):
    return type('Enum', (), enums)




def make_thumbnail(file_name, width, extension, save_root):

    file_path = os.path.join(save_root,'{}.{}'.format(file_name,extension))

    image = Image.open(file_path)
    if image.mode not in ('L', 'RGB'):
        image = image.convert('RGB')

    w, h = image.size

    max_side =  max(w, h)

    if width <= max_side*1.5:
        return file_path
    else:
        uploadRealFilePath = os.path.join(save_root,'{}_{}x{}.{}'.format(file_name,width,width,extension))

        image.thumbnail((width,width))

        image.save(uploadRealFilePath, format = EXTENSION_FORMAT[extension])

        return uploadRealFilePath




def iterfindfiles(path, fnexp):

    for root, dirs, files in os.walk(path):
    	for filename in fnmatch.filter(files, fnexp):
            yield os.path.join(root, filename)

def delete_file_and_thumbnails(file_name,save_root):

    name = os.path.splitext(file_name)[0]

    pattern = "{}*[.png|.jpg]".format(name)
    removed_files = list()

    for file_path in iterfindfiles(save_root, pattern):
        try :
            os.remove(file_path)
            removed_files.append(file_path)
        except:
            pass

    return removed_files