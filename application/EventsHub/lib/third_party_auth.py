__author__ = 'alexander'


class TencentAuth:
    @classmethod
    def json_key(self):
        return  "qq"

    @classmethod
    def user_key(self):
        return  "qq_openid"


class WeiboAuth:
    @classmethod
    def json_key(self):
        return  "weibo"

    @classmethod
    def user_key(self):
        return  "weibo_id"


class WechatAuth:
    @classmethod
    def json_key(self):
        return  "wechat"

    @classmethod
    def user_key(self):
        return  "wechat_openid"