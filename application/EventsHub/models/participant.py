__author__ = 'alexander'

from  .base import *
from .user import AccountModel


class ParticipantModel(Model):

    phone = StringType (default = '',max_length=30 )

    username = StringType (required=True,max_length = 40 )

    username_hash = MD5Type()

    note = StringType (default= "")

    date_reserve = DateTimeType()

    date_attend = DateTimeType()

    registered_user_ref = ObjectIdType()

    check = BooleanType(default=False)

    class Options:
        roles = {OutputRole: blacklist('_id'),CreateRole: blacklist('_id')}


class ParticipantsListModel(BaseModel):

    event_ref = ObjectIdType(required=True)

    count = IntType(default=0)

    participants_list = ListType(ModelType(ParticipantModel),default=[])

    class Options:
        roles = {OutputRole: blacklist('_id'),CreateRole: blacklist('_id')}


