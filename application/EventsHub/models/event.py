__author__ = 'alexander'

from  .base import *
from .address import AddressModel
from .user import SimpleUserModel

InReView = "In Review"
Available = "Available"
Closed = "Closed"
Ended = "Ended"

STATES = [InReView, Available, Closed, Ended]


class EventConfig(Model):

    phone_required = BooleanType(required=True,default=False)

    notify_on_join = BooleanType(required=True,default=False)




class EventModel(BaseModel):

    owner = ObjectIdType(required=True)

    owner_name = StringType(max_length = 50,min_length = 1)

    title = MultilingualStringType(max_length = 50,min_length = 1,required=True,default_locale=DEFAULT_LOCALE,locale_regex=MOBILE_LOCALE_REGEX)

    contact_phone = StringType (required=True,max_length=30)

    description = MultilingualStringType(required=True,default_locale=DEFAULT_LOCALE,locale_regex=MOBILE_LOCALE_REGEX)

    address = ModelType(AddressModel,required=True )

    pio = StringType(max_length=100)

    tags = ListType(IntType,required=True,default=list())

    total_participants_limit = IntType (default=0)

    fee_description = StringType(max_length=50)

    date_start = DateTimeType(required=True)

    date_end = DateTimeType(required=True)


    posters = ListType(StringType,default=list(),max_size=3)

    config = ModelType(EventConfig,required=True)

    # should not set
    participant_count = IntType (default= 0)
    like_count = IntType (default= 0)
    comments_count =  IntType (default= 0)


    #not using
    privacy_state = IntType(default=0,choices=(0,1,2))

    date_create = DateTimeType(default=datetime.datetime.now)

    state = StringType(choices=STATES, default=Available)

    class Options:
        roles = {OutputRole: blacklist(),
                 CreateRole: blacklist('_id', 'date_create','like_count','participant_count'),
                 UpdateRole: blacklist('_id', 'date_create','posters','state','like_count','participant_count',"owner")}






class EventLikeModel(BaseModel):

    like_date = DateTimeType(required=True,default=datetime.datetime.utcnow())

    user_ref = ObjectIdType(required=True)

    class Options:
        roles = {OutputRole: blacklist('_id'),CreateRole: blacklist('_id')}
class EventLikeListModel(BaseModel):

    event_ref = ObjectIdType(required=True)

    count = IntType(required=True,default=0)

    likes = ListType(ModelType(EventLikeModel),default=[])

    class Options:
        roles = {OutputRole: blacklist('_id'),CreateRole: blacklist('_id')}




class EventCommentModel(BaseModel):

    parent_id = ObjectIdType(required=True)

    event_ref = ObjectIdType(required=True)

    comment_date = DateTimeType(required=True,default=datetime.datetime.utcnow())

    user = ModelType(SimpleUserModel,required=True)

    comment_text = StringType(max_length=500)


    class Options:
        roles = {OutputRole: blacklist(),
                 CreateRole: blacklist('_id')}


