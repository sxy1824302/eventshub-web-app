__author__ = 'alexander'
from  .base import *

class Tag(BaseModel):

    tag = MultilingualStringType(max_length = 20,min_length = 1,required=True,default_locale=DEFAULT_LOCALE)

    tag_id = IntType (required=True)

    pinyin = StringType(required= True)

    class Options:
        roles = {OutputRole: blacklist('_id'),CreateRole: blacklist('_id')}

