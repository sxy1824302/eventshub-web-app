__author__ = 'sunxiangyu'
from  .base import *

class CityModel(BaseModel):

    name_hash = StringType()

    city_name = MultilingualStringType(max_length = 20,min_length = 1,required=True,default_locale=DEFAULT_LOCALE,locale_regex=MOBILE_LOCALE_REGEX)

    events_count = IntType(default=0)

    class Options:
        roles = {OutputRole: blacklist("_id", "events_count"),
                 CreateRole: blacklist('_id', 'events_count'),
                 UpdateRole: blacklist('_id', 'events_count')}

