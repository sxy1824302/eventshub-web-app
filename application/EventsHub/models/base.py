
from schematics.models import Model
import uuid
import datetime
from schematics.types import StringType,EmailType, IntType, BooleanType, UUIDType, GeoPointType,IPv4Type,MultilingualStringType,URLType,DateTimeType,MD5Type
from schematics.types.compound import ListType, ModelType,DictType
from schematics.exceptions  import ValidationError
from schematics.transforms import  blacklist
from ..localizable import  *
from schematics.contrib.mongo import ObjectIdType


CreateRole = 'create'
OutputRole = 'output'
UpdateRole = 'update'


MOBILE_LOCALE_REGEX = "^(([a-z]{2}|[a-z]{2}-[A-Z]{1}[a-z]+)(:?_[A-Z]{2})?)$"

class BaseModel(Model):
    _id = ObjectIdType()




