

from .base import *

class AddressModel(Model):
    street1 = StringType (required=True,max_length = 60 )

    street2 = StringType (max_length = 60 )

    city = StringType ( required=True,max_length = 60)

    province = StringType (required=True)

    country = StringType (required=True,default="CN" )

    geopoint = GeoPointType()

    district =StringType (max_length = 60 )




