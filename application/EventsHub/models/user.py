
from  .base import *

class SimpleUserModel (Model):
    ref = ObjectIdType(required=True)
    username = StringType (max_length = 40,serialize_when_none = False)
    avatar_path = StringType(serialize_when_none = False)
    gender = StringType(serialize_when_none = False)


    class Options:
        roles = {OutputRole: blacklist(),
                 CreateRole: blacklist('_id')}

class AccountModel(BaseModel):
    username = StringType (max_length = 40,serialize_when_none = False)

    email = EmailType (serialize_when_none = False)
 
    password = StringType (min_length = 6,max_length = 30,serialize_when_none = False)

    phone = StringType (max_length=30,serialize_when_none = False)

    date_create = DateTimeType(required=True,default=datetime.datetime.now)

    avatar_path = StringType(serialize_when_none = False)

    gender = StringType(serialize_when_none = False)

    birthday = DateTimeType(serialize_when_none = False)

    location_simple = StringType(serialize_when_none = False)

    third_party_credentials = DictType(StringType,StringType,serialize_when_none=False,default=dict())

    #do not change this manually
    created_events_count = IntType(default=0)

    activated = BooleanType (default = True)

    accountType  = StringType (default = "Individual",choices=["Individual","Shop"])

    class Options:
        roles = {OutputRole: blacklist('password','third_party_credentials'),
                 CreateRole: blacklist('_id','date_create'),
                 UpdateRole: blacklist('_id','date_create',"activated","session_token","password","avatar_path","created_events_count")}


class UserDetailModel(BaseModel):

    user_ref = ObjectIdType(required=True)

    liked_events_list = ListType(ObjectIdType,default=list())

    joint_events_list = ListType(ObjectIdType,default=list())

    class Options:
        roles = {OutputRole: blacklist(),CreateRole: blacklist('_id')}


class LoginTrackModel(BaseModel):

    user_ref = ObjectIdType(required=True)

    UDID = StringType(required=True, max_length=80)

    apns_token = StringType(required=True, max_length=80)

    ip = IPv4Type()

    access_instance_token = StringType(required=True)

    class Options:
        roles = {CreateRole: blacklist('_id')}
