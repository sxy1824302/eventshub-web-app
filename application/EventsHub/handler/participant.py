__author__ = 'alexander'

from .base import *

class MyJointEventsHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):

        detail = None

        cursor = self.db.user_details.find({'user_ref': self.authorized_user_id}).limit(1)

        while (yield cursor.fetch_next):
            detail = cursor.next_object()

        if detail:

            cursor = self.db.events.find({'_id':{"$in":detail["joint_events_list"]}})

            participants = yield cursor.to_list(None)

            result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), participants)

            self.write({"events":list(result)})
            self.finish()

        else:
            self.write_error_json_message("user data not fount"+str(self.authorized_user_id))