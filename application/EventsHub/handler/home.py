__author__ = 'alexander'
from .base import *



class HomeHandler(RequestHandler):
    def get(self, *args, **kwargs):
        self.write("Welcome!")


class AppDownloadHandler(RequestHandler):
    def get(self, *args, **kwargs):
        self.redirect("https://itunes.apple.com/us/app/ju-ba/id915681805?ls=1&mt=8")
