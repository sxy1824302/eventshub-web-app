__author__ = 'alexander'
from .base import *
from werkzeug.security import check_password_hash,generate_password_hash
from ..lib.utils import enum
from ..lib.third_party_auth import *
from pymongo.errors import DuplicateKeyError

RegisterCode = enum(Success=1, Failed=0, Exist=5)




def parseRegisterJson(json_args):

    login_device_id = json_args.get("login_device_id",None)

    username = json_args.get("username",None)

    id = json_args.get("id",None)

    type = json_args.get("login_type",None)

    return (login_device_id,username,id,type)

def parseLoginJson(json_args):
    login_device_id = json_args.get("login_device_id",None)

    id = json_args.get("id",None)

    type = json_args.get("login_type",None)

    return (login_device_id,id,type)

class ThirdPartyRegisterHandler(AppHandler):

  @tornado.gen.coroutine
  @tornado.web.asynchronous
  def post(self):

    login_device_id,username ,id, type = parseRegisterJson(self.json_args)

    try:
        user = None

        if type == TencentAuth.json_key():
            confirm_key = TencentAuth.user_key()
            confirm_value = self.json_args.get("id",None)
        elif type == WeiboAuth.json_key():
            confirm_key = WeiboAuth.user_key()
            confirm_value = self.json_args.get("id",None)
        elif type == WechatAuth.json_key():
            confirm_key = WechatAuth.user_key()
            confirm_value = self.json_args.get("id",None)
        else:
            raise HTTPError(404,"login type not supported")

        cursor = self.db.users.find({"third_party_credentials.{}".format(confirm_key): confirm_value}).limit(1)

        while (yield cursor.fetch_next):
            user = cursor.next_object()
        if user:
            raise ValidationError('user_exists')

    except ValidationError as e:
        if  'user_exists' in e.messages:
            self.write_error_json_message('USER_EXIST_REGISTRATION',RegisterCode.Exist)
            return

        self.write_error_json_message(message= e.messages,code=RegisterCode.Failed)
    else:

        user = AccountModel()

        user.username = username
        user.third_party_credentials[confirm_key] = id
        result = yield self.db.users.insert(user.to_native(role=CreateRole))

        user._id = ObjectId( result)

        detail = UserDetailModel()

        detail.user_ref = user._id

        result = yield self.db.user_details.insert(detail.to_native(role=CreateRole))


        login_track = LoginTrackModel()
        login_track.user_ref  = user._id
        login_track.UDID = login_device_id
        login_track.ip = self.request.remote_ip
        login_track.access_instance_token = str(uuid.uuid4())

        result =  yield self.db.login_track.update({"user_ref":user._id},{'$set':login_track.to_native(role=CreateRole)},upsert = True)


        self.finish({"user":user.to_primitive(role=OutputRole),"token":login_track.access_instance_token})

class LoginHandler(AppHandler):

  @tornado.gen.coroutine
  @tornado.web.asynchronous
  def post(self):

    login_device_id,confirm_value,auth_type = parseLoginJson(self.json_args)

    confirm_key = None

    if auth_type == TencentAuth.json_key():
        confirm_key = TencentAuth.user_key()
    elif auth_type == WeiboAuth.json_key():
        confirm_key = WeiboAuth.user_key()
    elif auth_type == WechatAuth.json_key():
        confirm_key = WechatAuth.user_key()
    else:
        raise HTTPError(404,"login type not supported")

    cursor = self.db.users.find({"third_party_credentials.{}".format(confirm_key): confirm_value}).limit(1)
    user = None
    while (yield cursor.fetch_next):
         user = cursor.next_object()

    message = None

    if user:

      if user['activated']:

        token = str(uuid.uuid4())

        result =  yield self.db.login_track.update({"user_ref":user["_id"]},{'$set':{"ip":self.request.remote_ip,"access_instance_token":token, "UDID":login_device_id}},upsert = True)

        if self.check_update_result(result):

            outputUser = AccountModel(user)

            self.finish({"user":outputUser.to_primitive(role=OutputRole),"token":token})
            return
        else:

            message = "LOGIN_TOKEN_FAILED_GENERATE"

      else:
        message = "LOGIN_INACTIVE"
    else:
      message = "LOGIN_NOT_EXIST"

    self.write_error_json_message(message)

