__author__ = 'alexander'

from .base import *

class TagsHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):
        cursor = self.db.tags.find()

        tags = yield cursor.to_list(None)

        self.set_status(200)

        result = map(lambda e: Tag(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), tags)

        self.write({"tags":list(result)})
        self.finish()
