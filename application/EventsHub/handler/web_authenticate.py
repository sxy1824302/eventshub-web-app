__author__ = 'alexander'
from .base import *
from werkzeug.security import check_password_hash,generate_password_hash
from ..lib.utils import enum


def GenPassword(length):
    chars=string.ascii_letters+string.digits
    return ''.join([random.choice(chars) for i in range(length)])



class RegisterHandler(AppHandler):

  @tornado.gen.coroutine
  @tornado.web.asynchronous
  def post(self):

    try:
        cursor = self.db.users.find({'email': self.json_args.get("email")}).limit(1)
        user = None

        while (yield cursor.fetch_next):
            user = cursor.next_object()

        if user:
            raise ValidationError('exists')
    except ValidationError as e:
        if  'exists' in e.messages:
            self.write_error_json_message('email already been used',0)
            return
        self.write_error_json_message(e.messages,0)
    else:
        user = AccountModel(self.json_args)

        user.password = generate_password_hash(self.json_args["password"])

        result = yield self.db.users.insert(user.to_native(role=CreateRole))

        detail = UserDetailModel()

        detail.user_ref = result

        result = yield self.db.user_details.insert(detail.to_native(role=CreateRole))

        self.finish(user.to_primitive(role=OutputRole))


class EmailLoginHandler(AppHandler):

  @tornado.gen.coroutine
  @tornado.web.asynchronous
  def post(self):


    cursor = self.db.users.find({"email": self.json_args.get("email")}).limit(1)

    login_device_id = self.json_args.get("login_device_id",None)

    user = None

    while (yield cursor.fetch_next):
         user = cursor.next_object()

    message = None

    if user:

      if user['activated'] and check_password_hash(user["password"],self.json_args.get("password")):

        token = str(uuid.uuid4())

        result =  yield self.db.login_track.update({"user_ref":user["_id"]},{'$set':{"ip":self.request.remote_ip,"access_instance_token":token, "UDID":login_device_id}},upsert = True)

        logging.info(dict(result))

        if self.check_update_result(result):

            outputUser = AccountModel(user)

            self.finish({"user":outputUser.to_primitive(role=OutputRole),"token":token})
            return
        else:

            message = "LOGIN_TOKEN_FAILED_GENERATE"

      else:
        message = "LOGIN_INACTIVE"
    else:
      message = "LOGIN_NOT_EXIST"

    self.write_error_json_message(message)


# TODO: not using
class RequestNewPasswordHandler(AppHandler):

  def get(self):
    self.render('request_password.html', message='')


  def post(self):
    email = self.get_argument('email', False)
    if not email:
      return self.render('request_password.html',
        message='An email address is required')

    user = self.db.users.find_one({'email': email})
    if user:
      reset_hash = helpers.generate_md5()
      user = self.db.users_find_and_modify(
        {'email': email}, {
          '$set': {
            'reset_hash': reset_hash,
            'enabled' : True  },
          '$unset' : {'join_hash' : 1}},
        new = True)
      self.smtp.send( 'Reset password', 'reset_password.html',
        user.email, {'user':user._data})

    # Don't tell luser if an email exists or not
    self.redirect('/')


class ResetPasswordHandler(AppHandler):


  def get(self, reset_hash=''):
    self.render('authentication/reset_password.html', message='',
      reset_hash = reset_hash)


  def post(self, reset_hash=None):
    message = "Invalid Arguments" # predefine message
    reset_hash = self.get_argument('hash', False)
    password = self.get_argument('password', False)
    if reset_hash and password:
      password = bcrypt.hashpw(password, bcrypt.gensalt())
      user = self.db.users.find_and_modify(
        {'reset_hash': reset_hash},
        {'$set': {'password': password}},
        new=True)
      if user:
        self.smtp.send( 'Updated Password', 'reset_password.html',
          user.email, {'user' : user._data})
        return self.redirect('/login')
    return self.render('authentication/new_password.html',
      message = "Invalid Arguments", reset_hash = '' )