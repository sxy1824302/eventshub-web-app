__author__ = 'alexander'
from .base import *


class CityHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):
        cursor = self.db.cities.find()

        cities = yield cursor.to_list(None)
        #print cities
        self.set_status(200)

        result = map(lambda e: CityModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), cities)

        self.write({"cities":list(result)})
        self.finish()



