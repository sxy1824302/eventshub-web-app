__author__ = 'alexander'
from .base import *
import datetime

class EventCommentsHandler(AppHandler,PagingMixIn):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self,event_id):

        comment_on_id = self.json_args.get("parent_id")

        comment_text = self.json_args.get("comment_text")

        if not comment_text:
            raise  HTTPError(402)

        comment = EventCommentModel()

        comment.event_ref = ObjectId(event_id)


        cursor = self.db.users.find({'_id': self.authorized_user_id}).limit(1)

        user_data = None

        while (yield cursor.fetch_next):
            user_data = cursor.next_object()

        user = SimpleUserModel()
        user.ref = self.authorized_user_id
        user.username =  user_data["username"]
        user.avatar_path =  user_data["avatar_path"]
        user.gender =  user_data["gender"]

        comment.user = user
        comment.comment_text = comment_text

        if comment_on_id:
            comment.parent_id = ObjectId(comment_on_id)

        result = yield self.db.comments.insert(comment.to_native(role=CreateRole))

        if result:

            self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'comments_count': 1 }})

            self.write_success_json_message("done",1)
            return

        self.write_error_json_message("comment failed")


    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self,event_id):

        try:
            limit = int(self.get_argument("limit"))
        except:
            limit = self.default_page_count

        if(limit > self.default_page_count):
            limit = limit = self.default_page_count


        cursor = self.db.comments.find({"event_ref":ObjectId(event_id)}).sort([('comment_date', -1)]).limit(limit)

        comments = yield cursor.to_list(self.default_page_count)

        result = map(lambda e: EventCommentModel(e).to_primitive(role=OutputRole), comments)

        self.write({"comments":list(result)})

        self.finish()






class EventCommentOperatorHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def delete(self,event_id,comment_id):

        cursor = self.db.events.find({'_id': ObjectId(event_id)}).limit(1)

        event = None

        while (yield cursor.fetch_next):
            event = cursor.next_object()
        if not event:
            raise HTTPError(404,"event not found")

        result =  yield self.db.comments.remove({'_id': ObjectId(comment_id),"user.ref":{"$in":[self.authorized_user_id,event["owner"]]}})

        if result["n"] == 1:
            self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'comments_count': -1 }})

            self.write_success_json_message("deleted")

        else:
            raise  HTTPError(500,"failed to delete {}".format(event_id))