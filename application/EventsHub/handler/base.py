__author__ = 'alexander'
import functools
import tornado.gen
from tornado.web import RequestHandler,StaticFileHandler
import base64
import  json
from schematics.exceptions import ValidationError,ConversionError
from .. import encoder
from bson import ObjectId
import  simplejson
import logging
import motor
import  random
import os
import string
from tornado.escape import json_decode, json_encode
from tornado.web import  HTTPError
from ..lib.apns_client import notify
from EventsHub.localizable import *
import hashlib
from ..models.tags import *
from ..models.user import *
from ..models.city import *
from ..models.event import *
from ..models.participant import *
from ..models.city import *
from ..models.address import AddressModel
from .. import localizable

def authenticated_user(f):
  @functools.wraps(f)
  @tornado.gen.coroutine
  def wrapper(self, *args, **kwargs):
        if self.request.headers.get('Authorization'):

            key = self.request.headers['Authorization'][7:]

            cursor = self.db.login_track.find({'access_instance_token': key}).limit(1)

            existence = None

            while (yield cursor.fetch_next):
                existence = cursor.next_object()

            if existence:
                try :
                    self.authorized_user_id = existence["user_ref"];

                except (ConversionError , ValidationError) as e:
                    self.write_error_json_message("user auth model failed : %s" % e)
                else:
                    f(self, *args, **kwargs)
            else:
                self.set_status(401)
                self.write_error_json_message("Unauthorized User",-4)

        else:
            self.set_status(401)
            self.write_error_json_message("Need provide an auth infomation: "+f.__name__+" CLASS:"+self.__class__.__name__,-4)

  return wrapper



class AppHandler(RequestHandler):

    def initialize(self):
        self.locale_arg = self.get_argument("locale",DEFAULT_LOCALE)
        self.authorized_user_id = None


    def prepare(self):

        if self.request.headers.get("Content-Type",None) and self.request.headers["Content-Type"].find("application/json")!=-1 and len(self.request.body)>0:
            self.json_args = json_decode(self.request.body)
        else:
            self.json_args = None


    @property
    def _args(self):
        a =  { k: self.get_argument(k) for k in self.request.arguments }
        return a

    @property
    def db(self):
        return self.settings['db']
    @property
    def redis_connection(self):
        return self.settings['redis_connection']

    @property
    def local_test(self):
        return self.settings['port']==5000

    def write_error_json_message(self ,message,code=-1):
        if self._status_code == 200:
            self.set_status(417)
        self.finish({'message':message,"code":code})

    def write_success_json_message(self ,message,code=1):
        self.finish({'message':message,"code":code})

    def check_update_result(self,result):
        return  (result["updatedExisting"] and result["ok"]) or (result["upserted"] and result["ok"])


class PagingMixIn:
    def __init__(self):
        self.default_page_count = 20
        self.requested_after = None