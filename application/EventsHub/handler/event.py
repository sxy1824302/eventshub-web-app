__author__ = 'alexander'
from .base import *
import datetime








class LikedEventsHandler(AppHandler,PagingMixIn):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):

        cursor = self.db.user_details.find({'user_ref': self.authorized_user_id}).limit(1)

        detail = None

        while (yield cursor.fetch_next):
            detail = cursor.next_object()

        cursor = self.db.events.find({'_id':{"$in":detail["liked_events_list"]}}).limit(self.default_page_count)
        events = yield cursor.to_list(self.default_page_count)

        result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), events)

        self.write({"events":list(result)})
        self.finish()


class UserEventsHandler(AppHandler,PagingMixIn):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self,user_id):

        query = dict()

        query.update({"owner": ObjectId(user_id)})

        if len(query)==0:
            raise HTTPError(500,"unsatisfied query")
        else:
            cursor = self.db.events.find(query).sort([('date_end', -1)]).limit(self.default_page_count)

            posts = yield cursor.to_list(self.default_page_count)

            result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), posts)

            self.write({"events":list(result)})

            self.finish()

class CreatedEventsHandler(AppHandler,PagingMixIn):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):

        query = dict()

        query.update({"owner": self.authorized_user_id})

        if len(query)==0:
            raise HTTPError(500,"unsatisfied query")
        else:
            cursor = self.db.events.find(query).sort([('date_end', -1)]).limit(self.default_page_count)

            posts = yield cursor.to_list(self.default_page_count)

            result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), posts)
            self.write({"events":list(result)})
            self.finish()

class RecommendEventsHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):

        query = dict()

        startAt =datetime.datetime.utcnow()

        query.update({"date_end":{"$gt": startAt}})

        query.update({ "$where": "this.posters.length > 0"})

        #city = self.get_query_argument('city', None)

        if len(query)==0:
            self.write_error_json_message("unsatisfied query",0)
        else:
            cursor = self.db.events.find(query)

            cursor.sort([('participant_count',-1)]).limit(5)

            posts = yield cursor.to_list(5)

            result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), posts)

            self.write({"events":list(result)})
            self.finish()



class EventsOperationHandler(AppHandler):
    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def delete(self,event_id):

        result =  yield self.db.events.remove({'_id': ObjectId(event_id)})

        if result["n"] == 1:
            yield self.db.users.update({'_id': self.authorized_user_id},{'$inc': { 'created_events_count': -1 }})

            yield self.db.likes.remove({'event_ref': ObjectId(event_id)})

            yield self.db.participants.remove({'event_ref': ObjectId(event_id)})

            self.write_success_json_message("deleted")
        else:
            raise  HTTPError(500,"failed to delete {}".format(event_id))

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def put(self, event_id):
        obj_id = ObjectId(event_id)
        ret = yield self.db.events.find_one({'_id': obj_id, 'owner': self.authorized_user_id})
        if not ret:
            raise HTTPError(404,"id or owner is not exist")
        else:
            try:
                ret.update(self.json_args)
                update_event = EventModel(ret)
                update_event.validate()

                for create_locale,value in update_event.title.items():
                    if (create_locale!=DEFAULT_LOCALE):
                        update_event.title.update({DEFAULT_LOCALE:value})
                        update_event.description.update({DEFAULT_LOCALE:update_event.description[create_locale]})
                        break
            except (ConversionError , ValidationError) as e:
                self.write_error_json_message(e.message)
            else:
                update_info = update_event.to_native(role=UpdateRole)
                result = yield self.db.events.update({'_id': obj_id}, {'$set': update_info})

                if result['n'] > 0:
                    self.write_success_json_message("done")
                else:
                    message = "failed to update"
                    raise HTTPError(500,message)

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self, event_id):
        cursor = self.db.events.find({'_id': ObjectId(event_id)}).limit(1)


        event = None
        while (yield cursor.fetch_next):
            event = cursor.next_object()

        if event == None:
            raise HTTPError(404,"event not exist")
        result = EventModel(event)
        self.write(result.to_primitive(role=OutputRole,context={"locale":self.locale_arg}))



class EventsHandler(AppHandler,PagingMixIn):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self):

        try :

            event = EventModel(self.json_args)

            event.validate()

            for create_locale,value in event.title.items():
                if (create_locale!=DEFAULT_LOCALE):
                    event.title.update({DEFAULT_LOCALE:value})
                    event.description.update({DEFAULT_LOCALE:event.description[create_locale]})
                    break




        except (ConversionError , ValidationError) as e:
            self.write_error_json_message(e.message)
        else:

            result = yield self.db.events.insert(event.to_native(role=CreateRole))

            if result:
                 # prebuild the list

                list = EventLikeListModel({"event_ref":result})
                yield self.db.likes.insert(list.to_native(role= CreateRole))

                parti_list = ParticipantsListModel({"event_ref":result})
                yield self.db.participants.insert(parti_list.to_native(role= CreateRole))

                yield self.db.users.update({'_id': self.authorized_user_id},{'$inc': { 'created_events_count': 1 }})


                hash = hashlib.md5(event.address.city.encode("utf8")).hexdigest()

                name_dict = {DEFAULT_LOCALE:event.address.city}

                city = CityModel({"name_hash":hash,"city_name":name_dict})

                dict = city.to_native(role= CreateRole)

                self.db.cities.update({'name_hash': hash},{'$inc': { 'events_count': 1 },"$set":dict},upsert = True)


                self.finish({"_id":str(result)})
            else:
                self.write_error_json_message("failed to create")


    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):

        query = dict()

        endAt = self.get_query_argument('end_at',None)

        city = self.get_query_argument('city', None)

        self.requested_after = self.get_query_argument('after', None)

        try:

            after = datetime.datetime.strptime(self.requested_after,DateTimeType.SERIALIZED_FORMAT)

            if endAt:#time query

                    end = datetime.datetime.strptime(endAt,DateTimeType.SERIALIZED_FORMAT)

                    query.update({"date_end":{"$gt": after, "$lt": end}})

                    if end < datetime.datetime.utcnow():
                        raise HTTPError(417, 'you are search with a invalid end date {}'.format(endAt))
            else:

                query.update({"date_end":{"$gt": after}})



        except (ValueError) as e:
            raise  HTTPError(500,e.message)


        if city:
            query.update({'address.city': city})

        if len(query)==0:
             raise HTTPError(500, 'query not valid :{}'.format(query))
        else:

            cursor = self.db.events.find(query).sort([('date_end', 1),('address.city',1)]).limit(self.default_page_count)

            posts = yield cursor.to_list(self.default_page_count)

            result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), posts)

            self.write({"events":list(result)})

            self.finish()





class EventLikeHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def delete(self,event_id):

        result = yield self.db.likes.update(
        {'event_ref': ObjectId(event_id)},
        {'$inc': { 'count': -1 },
                    '$pull':{"likes":{'user_ref':self.authorized_user_id}}}
        )

        if self.check_update_result(result):
            self.db.user_details.update({'user_ref': self.authorized_user_id},{'$pull':{'liked_events_list':ObjectId(event_id)}})

            self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'like_count': -1 }})

            self.write_success_json_message("done",0)
            return

        self.write_error_json_message("cancel instrest failed")



    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self,event_id):

        like = EventLikeModel()
        like.like_date = datetime.datetime.utcnow()
        like.user_ref = self.authorized_user_id

        result = yield self.db.likes.update(
            {'event_ref': ObjectId(event_id), "likes.user_ref":{'$ne': self.authorized_user_id}},
            {'$inc': { 'count': 1 },
                        '$push':{'likes':like.to_native(role=CreateRole)}})

        if self.check_update_result(result):

            self.db.user_details.update({'user_ref': self.authorized_user_id},{'$addToSet':{'liked_events_list':ObjectId(event_id)}})

            self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'like_count': 1 }})

            self.write_success_json_message("done",1)
            return

        self.write_error_json_message("already liked")






    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self,event_id):

        cursor = self.db.likes.find({"$and":[{'event_ref': ObjectId(event_id)},
                                                 {'likes.user_ref':{"$in":[self.authorized_user_id]}}
        ]}).limit(1)

        like = None

        while (yield cursor.fetch_next):
            like = cursor.next_object()

        if like:
            self.write_success_json_message("Have a like",1)
        else:
            self.write_success_json_message("Dont have a like",0)




class SearchEventsHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self):
        query = self.get_query_argument('query')
        count = self.get_query_argument('count', default=20)
        lang = self.get_query_argument('lang', default='en_US')
        if lang == 'en_US':
            cursor = self.db.events.find({'$text': {'$search': query}})
        else:
            cursor = self.db.events.find({'title.{}'.format(lang): {'$regex': query}})

        events = yield cursor.limit(count).to_list(None)

        result = map(lambda e: EventModel(e).to_primitive(role=OutputRole,context={"locale":self.locale_arg}), events)

        self.write({"events":list(result)})
        self.finish()



class PageEventHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self, event_id):
        event = yield self.db.events.find_one({'_id': ObjectId(event_id)})
        result = self.get_argument('result', default=None)
        participants = yield self.db.participants.find_one({'event_ref': ObjectId(event_id)})
        if event:
            self.render("event.html", event=event, participants=participants, result=result)
        else:
            raise HTTPError(404,"event not found")
