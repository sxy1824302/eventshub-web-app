__author__ = 'alexander'

from .base import *


class UsersHandler(AppHandler):

    @tornado.gen.coroutine
    @tornado.web.asynchronous
    def get(self, user_id):
        ret = yield self.db.users.find_one({'_id': ObjectId(user_id)})
        if not ret:
            self.write_error_json_message("user id not exist")
        else:
            user = AccountModel(ret)
            self.finish(user.to_primitive(role=OutputRole))

    @authenticated_user
    @tornado.gen.coroutine
    @tornado.web.asynchronous
    def put(self, user_id):

        if ObjectId(user_id) != self.authorized_user_id:
            self.write_error_json_message("not allowed to modify someone else")
            return

        try:
            update_model = AccountModel(self.json_args)
            update_model.validate()
        except (ConversionError , ValidationError) as e:
            self.write_error_json_message(e.message)
        else:

            update_info = update_model.to_native(role=UpdateRole)

            if update_info.get("avatar_path",None) != None:
                self.write_error_json_message("illegal update form"+"avatar_path")
                return

            result = yield self.db.users.update({'_id': self.authorized_user_id}, {'$set':update_info})

            if result['n'] >0:
                self.write_success_json_message("done")
                return
            else:
                message = "failed to update"
            self.write_error_json_message(message)

def got_feedback(tuples):
    logging.info(tuples)

class UserDeviceHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def put(self, user_id):

        UDID = self.json_args.get('uuid')

        token = self.json_args.get('token')

        if user_id:
            result = yield self.db.login_track.update({'user_ref': ObjectId(user_id),"UDID":UDID},
                                                       {'$set': { 'apns_token': token}})
            self.write_success_json_message("done")
        else:
            self.write_error_json_message("uuid can't be empty")


