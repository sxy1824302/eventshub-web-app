# -*- coding: utf-8

__author__ = 'alexander'
from .base import *
import hashlib;
from  ..lib import notifications
from schematics.types import MD5Type
class PageJoinEventHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self, event_id, result=False):
        event =yield self.db.events.find_one( {'_id':ObjectId(event_id)})
        p_list = yield self.db.participants.find_one({'event_ref': ObjectId(event_id)})

        if event:
            self.render("join.html", event=event, participants=p_list, result=result)
        else:
            self.set_status(404)
            self.write_error_json_message("sorry, Event not found")


    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self, event_id):
        name = self.get_argument('inputNickname')
        phone = self.get_argument('inputPhone')
        # email = self.get_argument('inputEmail')

        result = {'status': "OK"}


        object_id = ObjectId(event_id)
        count =  yield self.db.participants.find({"participants_list.username": name, 'event_ref': object_id}).count()
        event =yield self.db.events.find_one( {'_id': object_id})
        p_list = yield self.db.participants.find_one({'event_ref': object_id})
        if event["total_participants_limit"]!=0 and p_list["count"] >= event["total_participants_limit"]:
            result['status'] = "ERROR"
            result['msg'] = "报名人数已满"
        elif count > 0:
            result['status'] = "ERROR"
            result['msg'] = "该昵称用户已经在参与人列表里"
        elif not name or (event['config']['phone_required'] and not phone):
            result['status'] = 'ERROR'
            result['msg'] = '请填写必要的信息，带 * 的为必填!'

        if event and result['status']=="ERROR":
            self.render("join.html", event=event, participants=p_list, result=result)
            return

        try:
          p = ParticipantModel({'username': name,
                              'phone': phone,
                              'note': '',})
          p.validate()

        except (ConversionError , ValidationError) as e:
            self.write_error_json_message(e.message)
        else:
            # Todo: fix duplicate insert

            p.username_hash = hashlib.md5(p.username.encode("utf8")).hexdigest()

            ret = yield self.db.participants.update(
            {'event_ref': object_id},#, "participants_list.phone":{'$ne': participant.phone}},
            {'$inc': { 'count': 1 },
             '$push':{'participants_list':p.to_native(role=CreateRole)}},upsert=True)

            self.db.events.update({'_id': object_id},{'$inc': { 'participant_count': 1 }})
            if result['status']=="OK":
                self.redirect("/page/event/{}?result=OK".format(event_id))
            # else:
            #     self.get(event_id, result=result)

class ParticipantHandler(AppHandler):

    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self,event_id,participant_ref):

        cursor = self.db.participants.find({'event_ref': ObjectId(event_id),'participants_list.registered_user_ref':{"$in":[ObjectId(participant_ref)]}}).limit(1)

        participant = None

        while (yield cursor.fetch_next):
            participant = cursor.next_object()
        if participant:
            self.write_success_json_message("joint",1)
        else:
            self.write_success_json_message("not joint",0)

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def delete(self,event_id,participant_ref):

        participant_ref = ObjectId(participant_ref)

        result = yield self.db.participants.update(
            {'event_ref': ObjectId(event_id)},
            {'$inc': { 'count': -1 },
             '$pull':{"participants_list":{'registered_user_ref':participant_ref}}}
            )

        if self.check_update_result(result):
            self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'participant_count': -1 }})
# todo: maybe leave this message
            self.db.user_details.update({'user_ref': participant_ref},{'$pull': { 'joint_events_list': ObjectId(event_id) }})

            self.write_success_json_message("quited")
            return
        self.write_error_json_message("failed to quiet event for this user")





class JoinEventHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self,event_id):


        event =yield self.db.events.find_one( {'_id':ObjectId(event_id)})

        parti_list = yield self.db.participants.find_one({'event_ref': ObjectId(event_id)})


        if event["total_participants_limit"]!=0 and parti_list["count"] >= event["total_participants_limit"]:
            self.write_error_json_message("event is full")
            return

        try:
            participant = ParticipantModel(self.json_args)
            participant.validate()
        except (ConversionError , ValidationError) as e:
            self.write_error_json_message(e.message)
        else:
            participant.username_hash = hashlib.md5(participant.username.encode("utf8")).hexdigest()

            if (self.json_args["registered_user_ref"])==None:

                result = yield self.db.participants.update(
                {'event_ref': ObjectId(event_id), "participants_list.username_hash":{'$ne': participant.username_hash.to_native()}},
                {'$inc': { 'count': 1 },
                '$push':{'participants_list':participant.to_native()}})

            else:
                result = yield self.db.participants.update(
                {'event_ref': ObjectId(event_id), "participants_list.registered_user_ref":{'$ne': participant.registered_user_ref}},
                {'$inc': { 'count': 1 },
                '$push':{'participants_list':participant.to_native()}})


            if self.check_update_result(result):

                if event["config"]["notify_on_join"]:

                    cursor = self.db.login_track.find({"user_ref": event["owner"]}).limit(1)
                    track = None
                    while (yield cursor.fetch_next):
                        track = cursor.next_object()
                    if track and track["apns_token"]:
                        locale,text = event["title"].popitem()
                        notify('com.uriphium.eventshub', track["apns_token"], {'aps':{'alert':localizable.MESSAGE_JOIN.format(participant["username"],text)}})



                self.db.user_details.update({'user_ref': self.authorized_user_id},{'$push': { 'joint_events_list': ObjectId(event_id) }})
                self.db.events.update({'_id': ObjectId(event_id)},{'$inc': { 'participant_count': 1 }})
                self.write_success_json_message("joint")
                return

            self.write_error_json_message("join failed, user already joint")


class EventParticipantsHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def get(self,event_id,number_of_participants = 20):

        if event_id:

                cursor = self.db.participants.find({'event_ref': ObjectId(event_id)}).limit(1)

                participants_record = None

                while (yield cursor.fetch_next):
                    participants_record = cursor.next_object()

                result = []
                for p in participants_record["participants_list"]:
                    p.pop('_id', '')
                    result.append(ParticipantModel(p).to_primitive(role=OutputRole, context={"locale":self.locale_arg}))

                self.write({"participants":list(result)})
                self.finish()

        else:
            raise tornado.web.HTTPError(404)

class ParticipantAttendHandler(AppHandler):
    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def put(self, event_id):

        check = self.json_args.get('check')
        registered_user_ref = self.json_args.get('user_id')
        username_hash = hashlib.md5(self.json_args.get('user_name').encode("utf8")).hexdigest()
        hash = MD5Type()

        if check is not None:

            if not check:
                date = None
            else:
                date = datetime.datetime.now()

            if registered_user_ref:
                result =  yield self.db.participants.update({'event_ref': ObjectId(event_id),"participants_list": {"$elemMatch": {"registered_user_ref":ObjectId(registered_user_ref)}}}
            ,{'$set': {'participants_list.$.check':bool(check),'participants_list.$.date_attend':date}})
            else:
                result =  yield self.db.participants.update({'event_ref': ObjectId(event_id),"participants_list": {"$elemMatch": {"$and":[{"registered_user_ref":None},{"username_hash":hash.to_native(username_hash)}]}}}
            ,{'$set': {'participants_list.$.check':bool(check),'participants_list.$.date_attend':date}})

            #result = yield self.db.participants.update({"$and":[{'event_ref': ObjectId(event_id)},{"$or":[{'participants_list.registered_user_ref': {"$in":[ObjectId(phone_or_id)]}},{'participants_list.phone': {"$in":[phone_or_id]}}]}]},{'$set': {'participants_list.$.check': check,'participants_list.$.date_attend':date}})
            print (result)
            if self.check_update_result(result):
                self.set_status(200)
                self.write_success_json_message("done")
            else:

                self.write_error_json_message("updated failed"+str(result))
        else:
            self.write_error_json_message("invalid json data")

