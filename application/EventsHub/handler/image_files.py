__author__ = 'alexander'
from PIL import Image
from .base import *
from rq import Queue
import time
from ..lib.utils import  *


from EventsHub.lib.utils import make_thumbnail

EXTENSION_CONTENT_TYPE = {"gif":'image/gif',"jpg":'image/jpeg', "png":'image/png'}

CONTENT_TYPE_EXTENSION =  {value:key for key, value in EXTENSION_CONTENT_TYPE.items()}




class TileHandler(AppHandler):

    @tornado.gen.engine
    @tornado.web.asynchronous
    def get(self, image,extension1, width, extension2):

        file_path = os.path.join(self.application.settings['file_path'],'{}.{}'.format(image,extension1))
        if not os.path.isfile(file_path):
            self.write_error_json_message('File {} not found'.format(file_path))
            return

        file_type = EXTENSION_CONTENT_TYPE.get(extension2,None)
        if file_type == None:
            self.write_error_json_message("invalid file type")
            return

        width = int(width)


        uploadRealFilePath = os.path.join(self.application.settings['file_path'],'{}_{}x{}.{}'.format(image,width,width,extension2))

        if os.path.exists(uploadRealFilePath):
            self.redirect("/images/{}".format(os.path.basename(uploadRealFilePath)))
        else:

            q = Queue(connection=self.redis_connection)

            job = q.enqueue(
             make_thumbnail,
             image,
            width,
            extension2,
            self.application.settings['file_path'],
            )

            ioloop_instance = tornado.ioloop.IOLoop.instance()
            while True:
                yield tornado.gen.Task(
                    ioloop_instance.add_timeout,
                    time.time() + 1
                )
                if job.result is not None:
                    thumbnail_filepath = job.result
                    break

            self.set_header('Content-Type', EXTENSION_CONTENT_TYPE[extension2])
            self.set_status(200)
            self.write(open(thumbnail_filepath, 'rb').read())
            self.finish()



def validate_file(file_info):

    fileContent = file_info['body']

    content_type= file_info['content_type']

    if content_type not in EXTENSION_CONTENT_TYPE.values():
        return (-1,"invalid content type",None)

    return (1,content_type,fileContent)


class AvatarUploadHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self):

        try:
            status,content_type,fileContent  = validate_file(self.request.files['image'][0])
        except KeyError as e:
            raise HTTPError(500,str(self.request))


        if status<0 :
            self.write_error_json_message(content_type)
            return

        cursor = self.db.users.find({'_id': self.authorized_user_id}).limit(1)

        user = None

        while (yield cursor.fetch_next):
            user = cursor.next_object()

        if user and user.get("avatar_path",None):

            old_file_path = os.path.join(self.application.settings['file_path'],user["avatar_path"])

            if  os.path.isfile(old_file_path):
                try:
                    os.remove(old_file_path)
                except:
                    raise HTTPError(500, 'File {} failed to delete'.format(old_file_path))



        uploadRealPath = self.application.settings['file_path']

        fileName = save_file(uploadRealPath,fileContent,CONTENT_TYPE_EXTENSION[content_type])

        result = yield self.db.users.update({'_id': self.authorized_user_id}, {'$set':{"avatar_path":fileName}})

        if not self.check_update_result(result):
            self.write_error_json_message("failed to update user profile")
            return

        self.write_success_json_message({"name":fileName})

def save_file(uploadRealPath,fileContent,extension):
        fileName = time.strftime("%H%M%S") + '%d' % datetime.datetime.now().microsecond + '.' + extension
        try:
            os.makedirs(uploadRealPath)
        except Exception:
            pass
        uploadRealFilePath = os.path.join(uploadRealPath, fileName)
        uploadedFile = open(uploadRealFilePath, "wb")
        uploadedFile.write(fileContent)
        uploadedFile.close()
        return fileName

class PosterHandler(AppHandler):

    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def post(self,event_id):

        cursor = self.db.events.find({'_id': ObjectId(event_id)}).limit(1)
        event = None
        while (yield cursor.fetch_next):
            event = cursor.next_object()

        if event and len(event["posters"])<3 and len(self.request.files['image'])>0:

            failed_list = list()

            added_list = list()

            uploadRealPath = self.application.settings['file_path']

            for idx,imageinfo in enumerate( self.request.files['image']):

                status,content_type,fileContent  = validate_file(imageinfo)

                if status!=-1:
                    name = save_file(uploadRealPath,fileContent,CONTENT_TYPE_EXTENSION[content_type])

                    result = yield self.db.events.update({'_id': ObjectId(event_id)}, {'$push':{"posters":name}})

                    added_list.append(name)
                else:
                    failed_list.append(idx)

            if len(failed_list)>0:
                self.write_error_json_message("failed to update poster: "+",".join(failed_list))
            else:
                self.finish({"files":added_list})
        else:
            self.write_error_json_message("event not exist or full posters")


    @authenticated_user
    @tornado.web.asynchronous
    @tornado.web.gen.coroutine
    def delete(self,event_id,poster):

        cursor = self.db.events.find({'_id': ObjectId(event_id)}).limit(1)
        event = None

        while (yield cursor.fetch_next):
            event = cursor.next_object()

        if event and len(event["posters"]) > 0 :

            result = yield self.db.events.update({'_id': ObjectId(event_id)}, {'$pop':{"posters":poster}})

            if self.check_update_result(result):
                q = Queue(connection=self.redis_connection)

                job = q.enqueue(
             delete_file_and_thumbnails,
             poster,
            self.application.settings['file_path'],
                 )

                ioloop_instance = tornado.ioloop.IOLoop.instance()
                while True:
                    yield tornado.gen.Task(
                        ioloop_instance.add_timeout,
                        time.time() + 1
                    )
                    if job.result is not None:
                        deleted_files = job.result
                        break

                if len(deleted_files)<1:
                    raise HTTPError(500, 'File {} not exist to delete'.format(poster))
                self.write_success_json_message({"files":deleted_files})
            else:
                self.write_error_json_message("failed to remove poster from events")
        else:
            self.write_error_json_message("event not exist or delete not valid")