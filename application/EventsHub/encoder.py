__author__ = 'alexander'
import logging
import datetime
import simplejson as json
import uuid
import bson
FORMAT = "%Y-%m-%d %H:%M:%S"

def datetime_from_string(datetime_string):
    return datetime.datetime.strptime(datetime_string, FORMAT)

class ObjectEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime(FORMAT)
        elif isinstance(obj, uuid.UUID):
            return str(obj)
        elif isinstance(obj, bson.ObjectId):
            return str(obj)
        else:
            return super(ObjectEncoder, self).default(obj)