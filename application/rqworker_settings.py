

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

# You can also specify the Redis DB to use
# REDIS_DB = 3
# REDIS_PASSWORD = 'very secret'

# Queues to listen on
QUEUES = ['high', 'normal', 'low','default']

