__author__ = 'alexander'



import motor
import tornado.ioloop
import tornado.web
from EventsHub.event_tags import TAGS
from EventsHub.models.tags import Tag
from  EventsHub.models.base import CreateRole
from tornado.options import define, options


@tornado.web.gen.coroutine
def reset_users():
    print("droping users collection")
    result = yield db.drop_collection("users")
    print (result)
    print("droping user_details collection")
    result = yield db.drop_collection("user_details")
    print (result)
    print("droping login_track collection")
    result = yield db.drop_collection("login_track")
    print (result)

@tornado.web.gen.coroutine
def clean_events():
    print("droping events collection")
    result = yield db.drop_collection("events")
    print (result)

    print("droping participants collection")
    result = yield db.drop_collection("participants")
    print (result)
    print("droping likes collection")
    result = yield db.drop_collection("likes")
    print (result)

    print("droping cities collection")
    result = yield db.drop_collection("cities")
    print (result)

@tornado.web.gen.coroutine
def reset_tags():
    print("droping tags collection")
    result = yield db.drop_collection("tags")

    tags = list()
    for tag_data in TAGS:
            tags.append(Tag(tag_data).to_native(role=CreateRole))
    status = yield db.tags.insert(tags)

    db.tags.ensure_index([ ('tag_id',1)], unique=True)

    print("done...")

    print(status)

def define_options(option_parser):

    def config_callback(path):
        option_parser.parse_config_file(path, final=False)


    option_parser.define('user', type=bool, default=True, group='Application')

    option_parser.define('events', type=bool, default=True, group='Application')

    option_parser.define('tags', type=bool, default=False, group='Application')



if __name__ == '__main__':

    print("start connecting db")

    normal_uri = 'mongodb://server_user:JKHGA9812&^aj@127.0.0.1:27000/EventsHub'

    db = motor.MotorClient(normal_uri).EventsHub

    if db:
        print("connected")


    define_options(options)
    options.parse_command_line()
    print("OPTIONS")
    print(options.as_dict())

    if options.user :
        tornado.ioloop.IOLoop.current().run_sync(reset_users)
    if options.events :
        tornado.ioloop.IOLoop.current().run_sync(clean_events)
    if options.tags :
        tornado.ioloop.IOLoop.current().run_sync(reset_tags)






